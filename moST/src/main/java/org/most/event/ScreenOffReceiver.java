/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  Copyright 2013-2018 Alma Mater Studiorum - Università di Bologna
 *
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */
package org.most.event;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import org.most.MoSTApplication;
import org.most.input.Input;

public class ScreenOffReceiver extends EventReceiver {

	private final static boolean DEBUG = false;
	private final static String TAG = ScreenOffReceiver.class.getSimpleName();

	@Override
	public IntentFilter getIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		return filter;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (DEBUG) {
			Log.d(TAG, "Screen locking detected");
		}
		// Screen On -> vote to turn on apponscreen input
		((MoSTApplication) context.getApplicationContext()).getInputsArbiter().setEventVote(Input.Type.APPONSCREEN, false);
	}
}