/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil;

import org.junit.Test;
import org.most.Controller;
import org.most.MoSTApplication;
import org.most.input.BatteryInput;
import org.most.input.Input;
import org.most.pipeline.Pipeline;
import org.most.utils.MathUtils;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testCoordinatesDistance(){
        double userlat = 40.76000000;
        double userlon = -73.98400000;
        double venuelat = 40.76000000;
        double venuelon = -73.98400000;
        assertEquals(0, MathUtils.getDistance(userlat,userlon,venuelat,venuelon),0);
    }
}