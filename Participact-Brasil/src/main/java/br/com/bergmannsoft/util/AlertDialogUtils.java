/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.com.bergmannsoft.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import br.com.bergmannsoft.dialog.BProgressDialog;
import br.udesc.esag.participactbrasil.R;

/**
 * Created by fabiobergmann on 9/29/16.
 */

public class AlertDialogUtils {

    public static void showAlert(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog dialog = createDialog(activity, activity.getString(R.string.alert), message, "OK", null, null, null);
                dialog.show();
            }
        });
    }

    public static void showMessage(final Activity activity, final String message, final String title) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog dialog = createDialog(activity, title, message, "OK", null, null, null);
                dialog.show();
            }
        });
    }

    public static AlertDialog createDialog(Activity activity, String title, String message,
                                       String positive, String negative,
                                       DialogInterface.OnClickListener positiveListener,
                                       DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message).setPositiveButton(positive,
                positiveListener);
        if (negative != null && negative.length() > 0) {
            builder.setNegativeButton(negative, negativeListener);
        }
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public static void showError(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BProgressDialog.hideProgressBar();
                AlertDialog alert = createDialog(activity, activity.getString(R.string.alert_error), message, "OK", null,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }, null);
                alert.show();
            }
        });

    }

}
