/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.com.bergmannsoft.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

public abstract class CustomDialog {

    protected static final String TAG = "CustomDialog";
    protected Context context;
    protected View customView;
    protected DialogInterface.OnClickListener positiveListener;
    protected DialogInterface.OnClickListener negativeListener;
    protected String positiveLabel;
    protected String negativeLabel;
    protected String title;
    protected Dialog mDialog;

    public CustomDialog(Context context, String title, View customView) {
        super();
        this.context = context;
        this.title = title;
        this.customView = customView;

    }

    protected AlertDialog createCustomDialog(String title, View view) {
        AlertDialog alert = new AlertDialog.Builder(context).create();
        if (title != null)
            alert.setTitle(title);
        alert.setView(view);
        if (positiveListener != null)
            alert.setButton(AlertDialog.BUTTON_POSITIVE, positiveLabel, positiveListener);
        if (negativeListener != null)
            alert.setButton(AlertDialog.BUTTON_NEGATIVE, negativeLabel, negativeListener);
        return alert;
    }

    public void setPositiveButton(String positiveLabel, DialogInterface.OnClickListener positiveListener) {
        this.positiveLabel = positiveLabel;
        this.positiveListener = positiveListener;
    }

    public void setNegativeButton(String negativeLabel, DialogInterface.OnClickListener negativeListener) {
        this.negativeLabel = negativeLabel;
        this.negativeListener = negativeListener;
    }

    public Dialog show() {
        mDialog = createCustomDialog(title, customView);
        mDialog.show();
        return mDialog;
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public void dismiss() {
        if (mDialog != null)
            mDialog.dismiss();
    }

}