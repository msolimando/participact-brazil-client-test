/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.com.bergmannsoft.application;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;

/**
 * Created by fabiobergmann on 1/7/15.
 */
public abstract class BaseFragment extends Fragment {

    protected static final String TAG = BaseFragment.class.getName();
    protected BApplication app;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        app = (BApplication) getActivity().getApplication();
        app.setBaseFragment(this);
    }

    @Override
    public void onDestroy() {
        app.setBaseFragment(null);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        app.setBaseFragment(this);
    }

    @Override
    public void onPause() {
        app.setBaseFragment(null);
        super.onPause();
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            default:
                return false;
        }
    }

//    protected void handlePush(int type, Push push) {
//        // @Override it!
//        Log.d(TAG, "handlePush " + push.getBundle().toString());
//        switch (type) {
//            default:
//        }
//    }
}