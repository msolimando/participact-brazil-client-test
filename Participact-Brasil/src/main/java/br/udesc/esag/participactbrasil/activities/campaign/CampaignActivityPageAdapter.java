/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.activities.campaign;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import br.udesc.esag.participactbrasil.ParticipActApplication;
import br.udesc.esag.participactbrasil.R;
import br.udesc.esag.participactbrasil.domain.persistence.TaskFlat;

public class CampaignActivityPageAdapter extends FragmentPagerAdapter {

    private TaskFlat task;
    private int fragmentsCount = 4;
    boolean hasSensingActions = true;
    boolean hasDirectActions = true;

    private List<Fragment> fragments = new ArrayList<>();
    private CampaignDetailsFragment detailsFragment;
    private CampaignTasksFragment tasksFragment;
    private CampaignSensorsFragment sensorsFragment;
    private CampaignResultsFragment resultsFragment;

    public CampaignActivityPageAdapter(FragmentManager fm, TaskFlat taskFlat) {
        super(fm);
        task = taskFlat;

        if(task.getDirectActions() != null && task.getDirectActions().size() <=0){
            fragmentsCount--;
            hasDirectActions = false;
        }

        if(task.getSensingActions() != null && task.getSensingActions().size() <=0){
            fragmentsCount--;
            hasSensingActions = false;
        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                detailsFragment = CampaignDetailsFragment.newInstance(task);
                return detailsFragment;

            case 1:
                if(hasDirectActions) {
                    tasksFragment = CampaignTasksFragment.newInstance(task);
                    return tasksFragment;
                }else if(hasSensingActions){
                    sensorsFragment = CampaignSensorsFragment.newInstance(task);
                    return sensorsFragment;
                }else{
                    resultsFragment = CampaignResultsFragment.newInstance(task);
                    return resultsFragment;
                }

            case 2:
                if(hasDirectActions && hasSensingActions) {
                    sensorsFragment = CampaignSensorsFragment.newInstance(task);
                    return sensorsFragment;
                } else{
                    resultsFragment = CampaignResultsFragment.newInstance(task);
                    return resultsFragment;
                }

            case 3:
                resultsFragment =  CampaignResultsFragment.newInstance(task);
                return resultsFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return fragmentsCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ParticipActApplication.getInstance().getString(R.string .details);
            case 1:
                if(hasDirectActions) {
                    return ParticipActApplication.getInstance().getString(R.string.my_tasks);
                }else if(hasSensingActions){
                    return ParticipActApplication.getInstance().getString(R.string.my_sensors);
                }else{
                    return ParticipActApplication.getInstance().getString(R.string.results);
                }

            case 2:
                if(hasDirectActions && hasSensingActions) {
                    return ParticipActApplication.getInstance().getString(R.string.sensors);
                } else{
                    return ParticipActApplication.getInstance().getString(R.string.results);
                }

            case 3:
                return ParticipActApplication.getInstance().getString(R.string.results);
        }
        return "";
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        if (resultsFragment != null) {
            resultsFragment.notifyDataSetChanged();
        }
        if (sensorsFragment != null) {
            sensorsFragment.notifyDataSetChanged();
        }
        if (detailsFragment != null) {
            detailsFragment.notifyDataSetChanged();
        }
        if (tasksFragment != null) {
            tasksFragment.notifyDataSetChanged();
        }
    }
}
