/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.network.request;

import android.content.Context;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import br.udesc.esag.participactbrasil.ParticipActConfiguration;
import br.udesc.esag.participactbrasil.domain.rest.SignUpRestResult;
import br.udesc.esag.participactbrasil.domain.rest.SignUpRestRequest;
import br.udesc.esag.participactbrasil.network.request.base.BaseSpringAndroidSpiceRequest;

/**
 * Created by fabiobergmann on 9/29/16.
 */

public class SignUpRequest extends BaseSpringAndroidSpiceRequest<SignUpRestResult> {

    private final Context context;
    private final SignUpRestRequest request;

    public SignUpRequest(Context context, SignUpRestRequest request) {
        super(SignUpRestResult.class);
        this.request = request;
        this.context = context;
    }

    @Override
    public SignUpRestResult loadDataFromNetwork() throws Exception {
        ResponseEntity<SignUpRestResult> responseEntity = getRestTemplate().exchange(
                ParticipActConfiguration.SIGNUP_URL,
                HttpMethod.POST,
                getRequest(context, this.request),
                SignUpRestResult.class
        );
        SignUpRestResult body = responseEntity.getBody();
        return body;
    }

    public String getRequestCacheKey() {
        return String.format("signup.%s", request.getEmail());
    }

}
