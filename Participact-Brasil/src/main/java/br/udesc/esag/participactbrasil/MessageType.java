/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil;

import br.com.bergmannsoft.application.BMessageType;

/**
 * Created by fabiobergmann on 18/10/16.
 */

public interface MessageType extends BMessageType {

    int GCM_NOTIFICATION_NEW_TASK = 1000;

    int CAMPAIGN_ACCEPTED = 1001;

    int EDIT_PROFILE = 1002;
    int EDIT_PROFILE_SAVED = 1003;
    int MANDATORY_CAMPAIGN_ACCEPTED = 1004;

    int TASK_UPDATED = 1005;
}
