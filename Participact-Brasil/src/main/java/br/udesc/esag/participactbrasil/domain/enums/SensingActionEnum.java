/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.domain.enums;

import br.udesc.esag.participactbrasil.ParticipActApplication;
import br.udesc.esag.participactbrasil.R;

public class SensingActionEnum {

    public static enum Type {
        DUMMY, AUDIO_CLASSIFIER, ACCELEROMETER, ACCELEROMETER_CLASSIFIER, RAW_AUDIO, AVERAGE_ACCELEROMETER, APP_ON_SCREEN, APPS_NET_TRAFFIC, BATTERY, BLUETOOTH, CELL, CONNECTION_TYPE, DEVICE_NET_TRAFFIC, GYROSCOPE, INSTALLED_APPS, LIGHT, LOCATION, MAGNETIC_FIELD, PHONE_CALL_DURATION, PHONE_CALL_EVENT, SYSTEM_STATS, WIFI_SCAN, TEST;

        /**
         * Converts an integer to a valid Pipeline type.
         *
         * @param value The integer to convert
         * @return The Type represented by <code>value</code>. If the conversion
         * fails, returns DUMMY.
         */
        public static Type fromInt(int value) {
            switch (value) {
                case 1:
                    return ACCELEROMETER;
                case 2:
                    return RAW_AUDIO;
                case 3:
                    return AVERAGE_ACCELEROMETER;
                case 4:
                    return APP_ON_SCREEN;
                case 5:
                    return BATTERY;
                case 6:
                    return BLUETOOTH;
                case 7:
                    return CELL;
                case 8:
                    return GYROSCOPE;
                case 9:
                    return INSTALLED_APPS;
                case 10:
                    return LIGHT;
                case 11:
                    return LOCATION;
                case 12:
                    return MAGNETIC_FIELD;
                case 13:
                    return PHONE_CALL_DURATION;
                case 14:
                    return PHONE_CALL_EVENT;
                case 15:
                    return ACCELEROMETER_CLASSIFIER;
                case 16:
                    return SYSTEM_STATS;
                case 17:
                    return WIFI_SCAN;
                case 18:
                    return AUDIO_CLASSIFIER;
                case 19:
                    return DEVICE_NET_TRAFFIC;
                case 20:
                    return APPS_NET_TRAFFIC;
                case 21:
                    return CONNECTION_TYPE;
                case 99:
                    return TEST;
                default:
                    return DUMMY;
            }
        }

        public static String fromIntToHumanReadable(int value) {
            switch (value) {
                case 1:
                    return ParticipActApplication.getInstance().getString(R.string.accelerometer_string);
                case 2:
                    return ParticipActApplication.getInstance().getString(R.string.audio_string);
                case 3:
                    return ParticipActApplication.getInstance().getString(R.string.accelerometer_string);
                case 4:
                    return ParticipActApplication.getInstance().getString(R.string.foreground_application_string);
                case 5:
                    return ParticipActApplication.getInstance().getString(R.string.battery_level_string);
                case 6:
                    return ParticipActApplication.getInstance().getString(R.string.bluetooth_string);
                case 7:
                    return ParticipActApplication.getInstance().getString(R.string.mobile_station_string);
                case 8:
                    return ParticipActApplication.getInstance().getString(R.string.giroscope_string);
                case 9:
                    return ParticipActApplication.getInstance().getString(R.string.installed_applications_string);
                case 10:
                    return ParticipActApplication.getInstance().getString(R.string.light_sensor_string);
                case 11:
                    return ParticipActApplication.getInstance().getString(R.string.geographic_location_string);
                case 12:
                    return ParticipActApplication.getInstance().getString(R.string.magnetometer_string);
                case 13:
                    return ParticipActApplication.getInstance().getString(R.string.call_duration_string);
                case 14:
                    return ParticipActApplication.getInstance().getString(R.string.phone_number_string);
                case 15:
                    return ParticipActApplication.getInstance().getString(R.string.physical_activity_string);
                case 16:
                    return ParticipActApplication.getInstance().getString(R.string.statistics_system_string);
                case 17:
                    return ParticipActApplication.getInstance().getString(R.string.wifi_string);
                case 18:
                    return ParticipActApplication.getInstance().getString(R.string.type_of_sound_microphone_string);
                case 19:
                    return ParticipActApplication.getInstance().getString(R.string.measure_volume_data_device_traffic_string);
                case 20:
                    return ParticipActApplication.getInstance().getString(R.string.measure_volume_data_applications_traffic_string);
                case 21:
                    return ParticipActApplication.getInstance().getString(R.string.connection_type_string);
                case 22:
                    return ParticipActApplication.getInstance().getString(R.string.physical_activity_string2);
                case 23:
                    return ParticipActApplication.getInstance().getString(R.string.recognition_activities_string);
                case 24:
                    return ParticipActApplication.getInstance().getString(R.string.dynamic_recognition_string);
                case 99:
                    return ParticipActApplication.getInstance().getString(R.string.test_string);
                default:
                    return ParticipActApplication.getInstance().getString(R.string.dummy_string);
            }
        }

        /**
         * Converts the Pipeline type to an integer.
         *
         * @return integer
         */
        public int toInt() {
            switch (this) {
                case ACCELEROMETER:
                    return 1;
                case RAW_AUDIO:
                    return 2;
                case AVERAGE_ACCELEROMETER:
                    return 3;
                case APP_ON_SCREEN:
                    return 4;
                case BATTERY:
                    return 5;
                case BLUETOOTH:
                    return 6;
                case CELL:
                    return 7;
                case GYROSCOPE:
                    return 8;
                case INSTALLED_APPS:
                    return 9;
                case LIGHT:
                    return 10;
                case LOCATION:
                    return 11;
                case MAGNETIC_FIELD:
                    return 12;
                case PHONE_CALL_DURATION:
                    return 13;
                case PHONE_CALL_EVENT:
                    return 14;
                case ACCELEROMETER_CLASSIFIER:
                    return 15;
                case SYSTEM_STATS:
                    return 16;
                case WIFI_SCAN:
                    return 17;
                case AUDIO_CLASSIFIER:
                    return 18;
                case DEVICE_NET_TRAFFIC:
                    return 19;
                case APPS_NET_TRAFFIC:
                    return 20;
                case CONNECTION_TYPE:
                    return 21;
                case TEST:
                    return 99;
                default:
                    return 0;
            }
        }

    }
}
