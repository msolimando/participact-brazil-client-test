/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.activities.questionnaire;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import br.udesc.esag.participactbrasil.domain.persistence.ActionFlat;
import br.udesc.esag.participactbrasil.domain.persistence.Question;
import br.udesc.esag.participactbrasil.activities.questionnaire.QuestionnaireFragment;
import br.udesc.esag.participactbrasil.activities.questionnaire.QuestionnaireInfoFragment;

/**
 * Created by felipe on 15/05/2016.
 */
public class QuestionnairePageAdapter extends FragmentStatePagerAdapter {

    private ActionFlat actionFlat;
    private List<Question> questionList;
    public QuestionnairePageAdapter(FragmentManager fm, ActionFlat actionFlat) {
        super(fm);
        this.actionFlat = actionFlat;
        this.questionList = actionFlat.getQuestions();
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return QuestionnaireInfoFragment.newInstance(actionFlat);
        }
        return QuestionnaireFragment.newInstance(this.questionList.get(position-1));
    }

    @Override
    public int getCount() {
        return questionList.size()+1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "INFO";
        }
        return "Questão "+position;
    }
}
