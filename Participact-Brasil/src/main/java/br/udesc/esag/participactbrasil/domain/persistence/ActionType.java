/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.domain.persistence;

public enum ActionType {
    SENSING_MOST, PHOTO, QUESTIONNAIRE, ACTIVITY_DETECTION;

    @Override
    public String toString() {
        switch (this) {
            case SENSING_MOST:
                return "Passive sensing";
            case PHOTO:
                return "Photo";
            case QUESTIONNAIRE:
                return "Questionnaire";
            case ACTIVITY_DETECTION:
                return "Activity detection";
            default:
                return "Unknown";
        }
    }

    public static ActionType convertFrom(br.udesc.esag.participactbrasil.domain.persistence.ActionType old) {
        if (old.name().equalsIgnoreCase(SENSING_MOST.name())) {
            return SENSING_MOST;
        }

        if (old.name().equalsIgnoreCase(PHOTO.name())) {
            return PHOTO;
        }

        if (old.name().equalsIgnoreCase(QUESTIONNAIRE.name())) {
            return QUESTIONNAIRE;
        }

        return null;
    }

}
