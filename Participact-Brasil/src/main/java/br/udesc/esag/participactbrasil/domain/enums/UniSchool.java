/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.domain.enums;

public enum UniSchool {
    AGRARIA_E_MEDICINA_VETERINARIA, ECONOMIA_MANAGEMENT_E_STATISTICA, FARMACIA_BIOTECNOLOGIE_E_SCIENZE_MOTORIE, GIURISPRUDENZA, INGEGNERIA_E_ARCHITETTURA, LETTERE_E_BENI_CULTURALI, LINGUE_E_LETTERATURE_TRADUZIONE_E_INTERPRETAZIONE, MEDICINA_E_CHIRURGIA, PSICOLOGIA_E_SCIENZE_DELLA_FORMAZIONE, SCIENZE, SCIENZE_POLITICHE;
}
