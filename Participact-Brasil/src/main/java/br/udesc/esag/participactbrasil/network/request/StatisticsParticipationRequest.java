/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.network.request;

import android.content.Context;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import br.udesc.esag.participactbrasil.ParticipActConfiguration;
import br.udesc.esag.participactbrasil.domain.rest.*;
import br.udesc.esag.participactbrasil.domain.rest.StatisticsRequest;
import br.udesc.esag.participactbrasil.network.request.base.BaseSpringAndroidSpiceRequest;
import br.udesc.esag.participactbrasil.support.preferences.UserAccountPreferences;

/**
 * Created by fabiobergmann on 17/10/16.
 */

public class StatisticsParticipationRequest extends BaseSpringAndroidSpiceRequest<StatisticsResult> {

    private final StatisticsRequest request;
    private Context context;

    public StatisticsParticipationRequest(Context context, br.udesc.esag.participactbrasil.domain.rest.StatisticsRequest request) {
        super(StatisticsResult.class);
        this.context = context;
        this.request = request;
    }

    @Override
    public StatisticsResult loadDataFromNetwork() throws Exception {
        ResponseEntity<StatisticsResult> responseEntity = getRestTemplate().exchange(
                ParticipActConfiguration.STATISTICS2_URL + request.getTaskId(),
                HttpMethod.GET,
                getRequest(context, request),
                StatisticsResult.class
        );
        StatisticsResult body = responseEntity.getBody();
        return body;
    }

    public String getRequestCacheKey() {
        return String.format("stats.%s", UserAccountPreferences.getInstance(context)
                .getUserAccount().getUsername());
    }

}