/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import br.udesc.esag.participactbrasil.R;

/**
 * Created by felipe on 02/04/2016.
 */
public class PicturesListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<String> photosPathList;
    public PicturesListAdapter(Context context, List<String> photosList){
        this.context = context;
        this.photosPathList = photosList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return photosPathList.size();
    }

    @Override
    public Object getItem(int position) {
        return photosPathList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView==null){
            convertView = inflater.inflate(R.layout.list_item_picture, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imgViewPicture = (ImageView) convertView.findViewById(R.id.imgViewListItemPicture);

            ImageLoader.getInstance().displayImage("file:/"+photosPathList.get(position),viewHolder.imgViewPicture);

            convertView.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }

    static class ViewHolder {
        ImageView imgViewPicture;
    }
}
