/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.udesc.esag.participactbrasil.support.CheckClientAppVersionAlarm;
import br.udesc.esag.participactbrasil.support.ProgressAlarm;
import br.udesc.esag.participactbrasil.support.UploadAlarm;

public class ChangeDateBroadcastReceiver extends BroadcastReceiver {

    private final static Logger logger = LoggerFactory.getLogger(ChangeDateBroadcastReceiver.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        try {

            logger.warn("Changed date to {}", new DateTime());
            UploadAlarm.getInstance(context).stop();
            ProgressAlarm.getInstance(context).stop();
            CheckClientAppVersionAlarm.getInstance(context).stop();

        } finally {

            UploadAlarm.getInstance(context).start();
            ProgressAlarm.getInstance(context).start();
            CheckClientAppVersionAlarm.getInstance(context).start();

        }
    }

}
