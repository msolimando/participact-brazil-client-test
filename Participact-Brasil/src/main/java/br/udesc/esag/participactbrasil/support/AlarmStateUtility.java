/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.support;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import br.udesc.esag.participactbrasil.ParticipActApplication;
import br.udesc.esag.participactbrasil.broadcastreceivers.AlarmBroadcastReceiver;

public class AlarmStateUtility {

    public final static String CONSOLE_SUSPEND_INTENT = "br.udesc.esag.participactbrasil.participact.CONSOLE_SUSPEND_INTENT";
    public final static String KEY_TASK = "CONSOLE_SUSPEND_INTENT.KEY_TASK";

    private final static long SUSPEND_DURATION = 1000 * 60 * 60 * 8;

    public static synchronized void addAlarm(Context context, long taskId) {
        try {
            ParticipActApplication application = (ParticipActApplication) context.getApplicationContext();

            AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            Intent intent = new Intent();
            intent.setAction(CONSOLE_SUSPEND_INTENT + taskId);
            intent.putExtra(KEY_TASK, taskId);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mgr.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + SUSPEND_DURATION, pendingIntent);

            if (!application.getAlarmBR().containsKey(taskId)) {
                AlarmBroadcastReceiver alarm = new AlarmBroadcastReceiver(taskId);
                application.getAlarmBR().put(taskId, alarm);
                IntentFilter filter = new IntentFilter(CONSOLE_SUSPEND_INTENT + taskId);
                context.registerReceiver(alarm, filter);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public static synchronized void removeAlarm(Context context, long taskId) {
        try {
            ParticipActApplication application = (ParticipActApplication) context.getApplicationContext();

            AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent();
            intent.setAction(CONSOLE_SUSPEND_INTENT + taskId);
            intent.putExtra(KEY_TASK, taskId);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mgr.cancel(pendingIntent);

            if (application.getAlarmBR().containsKey(taskId)) {
                AlarmBroadcastReceiver alarm = application.getAlarmBR().get(taskId);
                context.unregisterReceiver(alarm);
                application.getAlarmBR().remove(taskId);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

}
