/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.activities.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import br.com.bergmannsoft.util.Connectivity;

/**
 * Created by fabiobergmann on 13/10/16.
 */

public class Settings {

    private static Settings instance;
    private Context context;

    private Settings(Context context) {
        this.context = context;
    }

    public static Settings getInstance(Context context) {
        if (instance == null) {
            instance = new Settings(context);
        }
        return instance;
    }

    public void setUseWiFiOnly(boolean b) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("use_wifi_only", b);
        editor.commit();
    }

    public boolean isUseWiFiOnly() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return preferences.getBoolean("use_wifi_only", false);
    }

    public boolean canSendData() {
        boolean wifiOnly = isUseWiFiOnly();
        return !wifiOnly || Connectivity.isWiFiConnected(context);
    }
}
