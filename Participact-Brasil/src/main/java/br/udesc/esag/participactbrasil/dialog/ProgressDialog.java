/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.dialog;

import android.app.Activity;
import android.util.Log;

import br.com.bergmannsoft.dialog.BProgressDialog;

/**
 * Created by fabiobergmann on 9/30/16.
 */

public class ProgressDialog {

    private static final String TAG = ProgressDialog.class.getSimpleName();

    public static void show(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BProgressDialog.showProgressBar(activity, false, message);
            }
        });
    }

    public static void updateMessage(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                BProgressDialog.updateMessage(message);
            }

        });
    }

    public static void dismiss(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    BProgressDialog.hideProgressBar();
                } catch (Exception e) {
                    Log.e(TAG, null, e);
                }
            }
        });
    }

}
