/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.domain.rest;

/**
 * Created by fabiobergmann on 9/29/16.
 */

public class SignUpRestRequest extends RestRequest {

    private String ageRange;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String device;
    private String city;
    private String country;
    private String photo;
    private String social;

    public SignUpRestRequest(String name, String surname, String email, String password, String device, String city, String country, String photo, String social, String ageRange) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.device = device;
        this.city = city;
        this.country = country;
        this.photo = photo;
        this.social = social;
        this.ageRange = ageRange;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getDevice() {
        return device;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getPhoto() {
        return photo;
    }

    public String getSocial() {
        return social;
    }

    public String getAgeRange() {
        return ageRange;
    }
}
