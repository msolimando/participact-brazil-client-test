/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.activities.dashboard;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.udesc.esag.participactbrasil.ParticipActApplication;
import br.udesc.esag.participactbrasil.R;

public class CampaignPageAdapter extends FragmentStatePagerAdapter {

    private CampaignActiveFragment campaignActiveFragment;
    private CampaignHistoryFragment campaignHistoryFragment;

    public CampaignPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            campaignActiveFragment = CampaignActiveFragment.newInstance();
            return campaignActiveFragment;
        } else {
            campaignHistoryFragment = CampaignHistoryFragment.newInstance();
            return campaignHistoryFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ParticipActApplication.getInstance().getResources().getString(R.string.enable);
            case 1:
                return ParticipActApplication.getInstance().getResources().getString(R.string.history);
        }
        return null;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        if (campaignActiveFragment != null) {
            campaignActiveFragment.performRequestForAvailableTasks();
        }
        if (campaignHistoryFragment != null) {
            campaignHistoryFragment.reloadTaskList();
        }
    }
}