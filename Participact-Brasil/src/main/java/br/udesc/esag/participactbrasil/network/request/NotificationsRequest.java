/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.network.request;

import android.content.Context;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import br.udesc.esag.participactbrasil.ParticipActConfiguration;
import br.udesc.esag.participactbrasil.domain.rest.MeRestResult;
import br.udesc.esag.participactbrasil.domain.rest.NotificationsResult;
import br.udesc.esag.participactbrasil.network.request.base.BaseSpringAndroidSpiceRequest;
import br.udesc.esag.participactbrasil.support.preferences.UserAccountPreferences;

public class NotificationsRequest extends BaseSpringAndroidSpiceRequest<NotificationsResult> {

    private Context context;

    public NotificationsRequest(Context context) {
        super(NotificationsResult.class);
        this.context = context;
    }

    @Override
    public NotificationsResult loadDataFromNetwork() throws Exception {
        ResponseEntity<NotificationsResult> responseEntity = getRestTemplate().exchange(ParticipActConfiguration.NOTIFICATIONS_URL, HttpMethod.GET, null, NotificationsResult.class);
        NotificationsResult body = responseEntity.getBody();
        return body;
    }

    public String getRequestCacheKey() {
        return String.format("me.%s", UserAccountPreferences.getInstance(context)
                .getUserAccount().getUsername());
    }

}