/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.domain.enums;

import br.udesc.esag.participactbrasil.ParticipActApplication;
import br.udesc.esag.participactbrasil.R;

public enum TaskState {
    AVAILABLE, ACCEPTED, RUNNING, REJECTED, FAILED, INTERRUPTED, ERROR, UNKNOWN, SUSPENDED, ANY, COMPLETED_NOT_SYNC_WITH_SERVER, COMPLETED_WITH_SUCCESS, COMPLETED_WITH_UNSUCCESS, RUNNING_BUT_NOT_EXEC, HIDDEN, GEO_NOTIFIED_AVAILABLE;

    //cant override toString because is used on the requests to the server
    public String toReadableString() {
        switch (this) {
            case AVAILABLE:
                return ParticipActApplication.getInstance().getString(R.string.available);

            case ACCEPTED:
                return ParticipActApplication.getInstance().getString(R.string.accept);

            case RUNNING:
                return ParticipActApplication.getInstance().getString(R.string.in_progress);

            case REJECTED:
                return ParticipActApplication.getInstance().getString(R.string.rejected);

            case FAILED:
                return ParticipActApplication.getInstance().getString(R.string.failed_campaign);

            case INTERRUPTED:
                return ParticipActApplication.getInstance().getString(R.string.stopped_campaign);

            case ERROR:
                return ParticipActApplication.getInstance().getString(R.string.error_campaign);

            case ANY:
            case UNKNOWN:
            case HIDDEN:
                return ParticipActApplication.getInstance().getString(R.string.not_available_status);

            case SUSPENDED:
                return ParticipActApplication.getInstance().getString(R.string.suspended);

            case COMPLETED_NOT_SYNC_WITH_SERVER:
                return ParticipActApplication.getInstance().getString(R.string.completed_not_synced);

            case COMPLETED_WITH_SUCCESS:
                return ParticipActApplication.getInstance().getString(R.string.completed_successfully);

            case COMPLETED_WITH_UNSUCCESS:
                return ParticipActApplication.getInstance().getString(R.string.concluded_failed);

            case RUNNING_BUT_NOT_EXEC:
                return ParticipActApplication.getInstance().getString(R.string.progress_not_running);

            case GEO_NOTIFIED_AVAILABLE:
                return ParticipActApplication.getInstance().getString(R.string.available_by_location);

            default:
                return ParticipActApplication.getInstance().getString(R.string.undefined);
        }
    }
}
