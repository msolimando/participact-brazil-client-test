/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.domain.rest;

import br.udesc.esag.participactbrasil.domain.rest.base.RestResult;

/**
 * Created by fabiobergmann on 17/10/16.
 */

public class MeRestResult extends RestResult {

    String name;
    String surname;
    String phone;
    String address;
    String photo;
    String currentZipCode;
    String currentNumber;
    String currentCity;
    String currentCountry;
    String currentProvince;
    String gender;
    String birthdate;

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getCurrentZipCode() {
        return currentZipCode;
    }

    public String getPhoto() {
        return photo;
    }

    public String getSurname() {
        return surname;
    }

    public String getCurrentNumber() {
        return currentNumber;
    }

    public String getCurrentCity() {
        return currentCity;
    }

    public String getCurrentCountry() {
        return currentCountry;
    }

    public String getCurrentProvince() {
        return currentProvince;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthdate() {
        return birthdate;
    }
}
