/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.support;

import android.content.Context;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import br.udesc.esag.participactbrasil.domain.local.UserAccount;
import br.udesc.esag.participactbrasil.network.request.LoginRequest;
import br.udesc.esag.participactbrasil.support.preferences.UserAccountPreferences;

/**
 * Created by fabiobergmann on 09/12/16.
 */

public class LoginRetry {

    public interface LoginRetryListener {
        void onSuccess();
        void onError();
    }

    public LoginRetry(Context context, SpiceManager contentManager, final LoginRetryListener listener) {

        UserAccount user = UserAccountPreferences.getInstance(context).getUserAccount();

        LoginRequest request = new LoginRequest(user.getUsername(), user.getPassword());
        contentManager.execute(request, request.createCacheKey(), DurationInMillis.ALWAYS_EXPIRED,
                new RequestListener<Boolean>() {
                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        listener.onError();
                    }

                    @Override
                    public void onRequestSuccess(Boolean aBoolean) {
                        listener.onSuccess();
                    }
                });
    }

}
