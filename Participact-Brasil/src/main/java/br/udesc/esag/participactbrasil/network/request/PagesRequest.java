/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.network.request;

import android.content.Context;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import br.udesc.esag.participactbrasil.ParticipActConfiguration;
import br.udesc.esag.participactbrasil.domain.rest.PagesRestRequest;
import br.udesc.esag.participactbrasil.domain.rest.PagesRestResult;
import br.udesc.esag.participactbrasil.domain.rest.SignUpRestRequest;
import br.udesc.esag.participactbrasil.domain.rest.SignUpRestResult;
import br.udesc.esag.participactbrasil.network.request.base.BaseSpringAndroidSpiceRequest;
import br.udesc.esag.participactbrasil.support.preferences.UserAccountPreferences;

/**
 * Created by fabiobergmann on 9/29/16.
 */

public class PagesRequest extends BaseSpringAndroidSpiceRequest<PagesRestResult> {

    private final Context context;
    private final PagesRestRequest request;

    public PagesRequest(Context context, PagesRestRequest request) {
        super(PagesRestResult.class);
        this.request = request;
        this.context = context;
    }

    @Override
    public PagesRestResult loadDataFromNetwork() throws Exception {
        ResponseEntity<PagesRestResult> responseEntity = getRestTemplate().exchange(
                ParticipActConfiguration.PAGES_URL,
                HttpMethod.GET,
                null,
                PagesRestResult.class
        );
        PagesRestResult body = responseEntity.getBody();
        return body;
    }

    public String getRequestCacheKey() {
        return String.format("pages.%s", UserAccountPreferences.getInstance(context).getUserAccount().getUsername());
    }

}
