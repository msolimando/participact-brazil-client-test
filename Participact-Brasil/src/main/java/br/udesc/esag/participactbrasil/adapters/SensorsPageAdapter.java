/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import br.udesc.esag.participactbrasil.ParticipActApplication;
import br.udesc.esag.participactbrasil.R;
import br.udesc.esag.participactbrasil.activities.campaign.CampaignSensorsFragment;
import br.udesc.esag.participactbrasil.activities.campaign.CampaignTasksFragment;
import br.udesc.esag.participactbrasil.domain.persistence.ActionFlat;
import br.udesc.esag.participactbrasil.domain.persistence.StateUtility;

/**
 * Created by felipe on 15/05/2016.
 */
public class SensorsPageAdapter extends FragmentStatePagerAdapter {

    private ArrayList<ActionFlat> actionFlatHistory;
    private ArrayList<ActionFlat> actionFlatPending;
    public SensorsPageAdapter(FragmentManager fm, Context context) {
        super(fm);

        actionFlatPending = new ArrayList<>(StateUtility.getAllOpenSensorsActions(context));
        actionFlatHistory = new ArrayList<>(StateUtility.getDoneSensorsActions(context));
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) {
            return CampaignSensorsFragment.newInstance(actionFlatPending, CampaignSensorsFragment.SensorsType.PENDING);
        }else{
            return CampaignSensorsFragment.newInstance(actionFlatHistory, CampaignSensorsFragment.SensorsType.HISTORY);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ParticipActApplication.getInstance().getString(R.string.pending);
            case 1:
                return ParticipActApplication.getInstance().getString(R.string.history);
        }
        return null;
    }
}
