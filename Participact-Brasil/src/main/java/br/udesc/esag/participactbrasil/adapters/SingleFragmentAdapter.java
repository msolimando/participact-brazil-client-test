/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.udesc.esag.participactbrasil.activities.notifications.FragmentNotifications;
import br.udesc.esag.participactbrasil.activities.profile.ProfileEditFragment;
import br.udesc.esag.participactbrasil.activities.profile.ProfileFragment;
import br.udesc.esag.participactbrasil.activities.settings.SettingsFragment;
import br.udesc.esag.participactbrasil.activities.webview.WebViewFragment;

public class SingleFragmentAdapter extends FragmentStatePagerAdapter {

    public enum SingleFragmentType {
        SETTINGS,
        PROFILE,
        PROFILE_EDIT,
        NOTIFICATION,
        HELP,
        ABOUT
    }

    private final Context context;
    private final SingleFragmentType type;

    private Fragment currentFragment;

    public SingleFragmentAdapter(FragmentManager fm, Context context, SingleFragmentType type) {
        super(fm);
        this.context = context;
        this.type = type;
    }

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (type) {
            case SETTINGS:
                currentFragment = SettingsFragment.newInstance();
                break;
            case PROFILE:
                currentFragment = ProfileFragment.newInstance();
                break;
            case PROFILE_EDIT:
                currentFragment = ProfileEditFragment.newInstance();
//                currentFragment = ProfileFragment.newInstance();
                break;
            case ABOUT:
                currentFragment = WebViewFragment.newInstance(WebViewFragment.WebViewType.ABOUT);
                break;
            case HELP:
                currentFragment = WebViewFragment.newInstance(WebViewFragment.WebViewType.HELP);
                break;
            case NOTIFICATION:
                currentFragment = FragmentNotifications.newInstance();
                break;
            default:
                return null;
        }
        return currentFragment;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

}
