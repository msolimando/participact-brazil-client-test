/*
 *  ParticipAct Brazil
 *  Copyright 2016-2019 Universidade do Estado de Santa Catarina - UDESC
 *  This file is part of ParticipAct Brazil.
 *  ParticipAct Brazil is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.
 *  ParticipAct Brazil is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with ParticipAct. If not, see <http://www.gnu.org/licenses/>.
 */

package br.udesc.esag.participactbrasil.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import br.udesc.esag.participactbrasil.domain.enums.TaskState;
import br.udesc.esag.participactbrasil.domain.persistence.StateUtility;
import br.udesc.esag.participactbrasil.domain.persistence.TaskFlat;
import br.udesc.esag.participactbrasil.domain.persistence.support.State;
import br.udesc.esag.participactbrasil.support.AlarmStateUtility;

public class AlarmBroadcastReceiver extends BroadcastReceiver implements Serializable {

    private final static Logger logger = LoggerFactory.getLogger(AlarmBroadcastReceiver.class);

    private static final long serialVersionUID = -2289366968646749313L;

    private long taskId;

    public AlarmBroadcastReceiver(long taskId) {
        this.taskId = taskId;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        long taskId = intent.getExtras().getLong(AlarmStateUtility.KEY_TASK);
        logger.info("Received resume alarm for task {}.", taskId);
        State state = StateUtility.loadState(context);

        if (state != null) {
            TaskFlat task = state.getTaskById(taskId).getTask();
            StateUtility.changeTaskState(context, task, TaskState.RUNNING);
            AlarmStateUtility.removeAlarm(context.getApplicationContext(), taskId);
        }

    }

    @Override
    public int hashCode() {
        return (int) taskId;
    }

}
