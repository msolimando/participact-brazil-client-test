# ParticipACT Brazil Client

# Overview
ParticipAct is a project by University of Bologna aimed
at studying the still under-explored potential of collaboration
among people exploiting smartphones as interaction tool and
interconnection medium.

We developed a smartphone application that allows users to easily
do coordinated tasks (for example, to automatically collect data
about network coverage or about audio pollution) and sends collected
data to our platform that process, aggregates and analyzes the data,
thus extracting information that could not be possibly be obtained
by a single user.

# Prerequisites
* The project is building with ```compileSdkVersion 21```
which requires JDK 7 or higher
* Android Studio (Tested Version 3.2.1)
* Gradle: Use default Wrapper

# Deploy and Run
1. Clone repository and open it with Android Studio
2. Wait for Gradle downloading dependencies (sync and build project)
3. Set the server IP address (```BASE_URL```) in the file ParticipActConfiguration